#############################################################################
# Makefile for building: kdeconnect-share.app/Contents/MacOS/kdeconnect-share
# Generated by qmake (3.1) (Qt 5.12.3)
# Project:  kdeconnect-share.pro
# Template: app
# Command: /Users/cici/IDE/Qt/5.12.3/clang_64/bin/qmake -o kdeconnect-share.xcodeproj/project.pbxproj kdeconnect-share.pro -spec macx-xcode
#############################################################################

MAKEFILE      = project.pbxproj

EQ            = =

MOC       = /Users/cici/IDE/Qt/5.12.3/clang_64/bin/moc
UIC       = /Users/cici/IDE/Qt/5.12.3/clang_64/bin/uic
LEX       = flex
LEXFLAGS  = 
YACC      = yacc
YACCFLAGS = -d
DEFINES       = -DQT_DEPRECATED_WARNINGS -DQT_NO_DEBUG -DQT_WIDGETS_LIB -DQT_GUI_LIB -DQT_DBUS_LIB -DQT_CORE_LIB
INCPATH       = -I. -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtGui.framework/Headers -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtDBus.framework/Headers -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtCore.framework/Headers -I. -I/Applications/Xcode10.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/OpenGL.framework/Headers -I/Applications/Xcode10.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/System/Library/Frameworks/AGL.framework/Headers/ -I/Users/cici/IDE/Qt/5.12.3/clang_64/mkspecs/macx-clang -F/Users/cici/IDE/Qt/5.12.3/clang_64/lib
DEL_FILE  = rm -f
MOVE      = mv -f

preprocess: compilers
clean preprocess_clean: compiler_clean

mocclean: compiler_moc_header_clean compiler_moc_objc_header_clean compiler_moc_source_clean

mocables: compiler_moc_header_make_all compiler_moc_objc_header_make_all compiler_moc_source_make_all

check: first

benchmark: first

compilers: moc_predefs.h moc_mainwindow.cpp
compiler_rcc_make_all:
compiler_rcc_clean:
compiler_moc_predefs_make_all: moc_predefs.h
compiler_moc_predefs_clean:
	-$(DEL_FILE) moc_predefs.h
moc_predefs.h: /Users/cici/IDE/Qt/5.12.3/clang_64/mkspecs/features/data/dummy.cpp
	/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/clang++ -pipe -stdlib=libc++ -O2 -std=gnu++11 -Wall -W -dM -E -o moc_predefs.h /Users/cici/IDE/Qt/5.12.3/clang_64/mkspecs/features/data/dummy.cpp

compiler_moc_header_make_all: moc_mainwindow.cpp
compiler_moc_header_clean:
	-$(DEL_FILE) moc_mainwindow.cpp
moc_mainwindow.cpp: mainwindow.h \
		/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtWidgets.framework/Headers/QMainWindow \
		moc_predefs.h \
		/Users/cici/IDE/Qt/5.12.3/clang_64/bin/moc
	/Users/cici/IDE/Qt/5.12.3/clang_64/bin/moc $(DEFINES) --include /Volumes/Storage/Inoki/GSoC/kdeconnect-share/kdeconnect-share/moc_predefs.h -I/Users/cici/IDE/Qt/5.12.3/clang_64/mkspecs/macx-clang -I/Volumes/Storage/Inoki/GSoC/kdeconnect-share/kdeconnect-share -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtWidgets.framework/Headers -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtGui.framework/Headers -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtDBus.framework/Headers -I/Users/cici/IDE/Qt/5.12.3/clang_64/lib/QtCore.framework/Headers -I/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include/c++/v1 -I/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/10.0.1/include -I/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include -I/Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.14.sdk/usr/include -F/Users/cici/IDE/Qt/5.12.3/clang_64/lib mainwindow.h -o moc_mainwindow.cpp

compiler_moc_objc_header_make_all:
compiler_moc_objc_header_clean:
compiler_moc_source_make_all:
compiler_moc_source_clean:
compiler_uic_make_all:
compiler_uic_clean:
compiler_rez_source_make_all:
compiler_rez_source_clean:
compiler_yacc_decl_make_all:
compiler_yacc_decl_clean:
compiler_yacc_impl_make_all:
compiler_yacc_impl_clean:
compiler_lex_make_all:
compiler_lex_clean:
compiler_clean: compiler_moc_predefs_clean compiler_moc_header_clean 

